package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        return (double) (Integer.parseInt(isbn) * 9) / 5 + 32;
    }
}
